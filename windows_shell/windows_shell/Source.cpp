#include "Source.h"


int main()
{
	size_t *found = new size_t();
	Helper *h = new Helper();
	while (true)
	{
		string cmd;
		cout << "$/";
		getline(cin, cmd);
		h->trim(cmd);
		if (cmd == "pwd")
			cout << pwd() << endl;
		else if (!cmd.find("cd"))
			cd(cmd);
		else if (!cmd.find("create"))
			create(cmd);
		else if (!cmd.find("ls"))
			ls(cmd);
		else if (!cmd.find("secret"))
			secret();
		else if (cmd.find(".exe") != std::string::npos)
			runExe(cmd);
	}
	return 0;
}

/*
This function returns the current directory
Input: none
Output: current directory
*/
LPSTR pwd()
{
	LPSTR buffer = new char[60];
	DWORD process = GetCurrentDirectoryA(60, buffer);
	if (process == 0)
	{
		cout << "Error: getPath" << endl;
	}
	return buffer;
}

/*
This function changes the directory
Input: directory path
Output: none
*/
void cd(string cmd)
{
	if (cmd.length() >= 3)
	{
		cmd = cmd.substr(3);
		LPCTSTR directory = cmd.c_str();
		if (!SetCurrentDirectoryA(directory))
			cout << "Error: changeDirectory" << endl;
	}
}

/*
This function creates a new file, if file already exist
it creates a new one.
Input: file path and name
Output: none
*/
void create(string cmd)
{
	HANDLE hfile;
	if (cmd.length() > 7)
	{
		cmd = cmd.substr(7);
		LPCTSTR file = cmd.c_str();
		hfile = CreateFileA(file, GENERIC_READ | GENERIC_WRITE,0, NULL, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL); // create_always new file
		if (hfile == INVALID_HANDLE_VALUE)
		{
			cout << "Error: openingFile" << endl;
		}
		CloseHandle(hfile);
	}
}

/*
This function prints names of all the files in the directory
Input: directory path, if the path is empty it returns current directory files names
Output: none
*/
void ls(string folder)
{
	vector<string> names;
	WIN32_FIND_DATA fd;
	if (folder.length() > 3)
		folder = folder.substr(3);
	else // if "folder" is empty
	{
		folder = folder.substr(2);
		folder = pwd();
	}
	string search_path = folder + "/*.*";
	HANDLE hFind = FindFirstFileA(search_path.c_str(), &fd);
	if (hFind != INVALID_HANDLE_VALUE) 
	{
		do {
			   if (!(fd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)) 
			   {
				   names.push_back(fd.cFileName); // add file's name to vector 
			   }
		   } 
		while (FindNextFileA(hFind, &fd));
		CloseHandle(hFind);
	}
	for (std::vector<string>::const_iterator i = names.begin(); i != names.end(); ++i)
		std::cout << *i << endl;
}

/*
This function loads "Secret.dll" and uses the function: "TheAnswerToLifeTheUniverseAndEverything"
Input: none
Output: none
*/
void secret()
{
	typedef int(*MYPROC)();
	MYPROC ProcAdd;
	HINSTANCE h;
	h = LoadLibraryA("Secret.dll");
	if (h != NULL)
	{
		ProcAdd = (MYPROC)GetProcAddress(h, "TheAnswerToLifeTheUniverseAndEverything");
		cout << ProcAdd() << endl;
		FreeLibrary(h);
	}
}

/*
This function runs exe file and prints the exit code when it ends
Input: file path
Output: none
*/
void runExe(string cmd)
{
	STARTUPINFO si;
	PROCESS_INFORMATION pi;
	DWORD ipExitCode;
	ZeroMemory(&si, sizeof(si));
	si.cb = sizeof(si);
	ZeroMemory(&pi, sizeof(pi));
	if (!CreateProcessA(NULL, const_cast<char *>(cmd.c_str()), NULL, NULL, FALSE, 0, NULL, NULL, &si, &pi)) // creates the procces
	{
		cout << "Error: opening .exe" << endl;
		return;
	}
	WaitForSingleObject(pi.hProcess, INFINITE); // �������� ���� ����� ������ ����� -> ������ ���� ����� ������� ���� ������ ������ ��� �� ��� ����� �����
	GetExitCodeProcess(pi.hProcess, &ipExitCode); // �������� ���� ������ �� ��� ������ �� ������� ���� ���� ������� ����� ��� �� ��� ������� ������ �� �������
	CloseHandle(pi.hProcess);
	CloseHandle(pi.hThread);
	cout <<"Exit code: " <<ipExitCode << endl;
}
